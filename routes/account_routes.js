const express = require("express");

const AccountRouter = express.Router();

const AccountModel = require("../models/Account");


AccountRouter.post("/addAccount", function(req, res){
	let newAccount = AccountModel({
		"name": req.body.name,
        "age": req.body.age,
        "address": req.body.address
        
	});

	newAccount.save(function (err, account){
		if(!err){
			return res.json({
				"account":account
			});
		}else {
			return res.send(err);
		}
	});

});

AccountRouter.get("/showAccounts", function(req , res){
	AccountModel.find({}).then(function (result){
		return res.json({"result":result})
	});
});

AccountRouter.put("/account/update/:id", function(req, res){
	AccountModel.update({"_id":req.params.id}, req.body).then(function (result){
		if(result){
			return res.json({"result":result})
		} else{
			return res.send("No account to update!")
		}
	});
});

AccountRouter.delete("/account/delete/:id", function(req, res){
	let idToDelete = req.params.id;

	AccountModel.findByIdAndRemove(idToDelete, function(err, result){
		if(!err){
			return res.json({"result":result})
		} else{
			return res.send(err);
		}
	})
})

module.exports = AccountRouter;