const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AccountSchema = new Schema({
	name: String,
	age: Number,
	address: String
});

module.exports = mongoose.model("Account", AccountSchema);